create or replace FUNCTION BEPAAL_VOORVEREISTE_CURSUS(
    p_courseno IN courses.course_no%TYPE
) RETURN VARCHAR AS 
    v_courseno      courses.course_no%TYPE;
    v_course_desc   courses.description%TYPE;
    v_prequisite    courses.prerequisite%TYPE;

    e_foutieve_no EXCEPTION;  
    PRAGMA EXCEPTION_INIT(e_foutieve_no,-2291);
BEGIN
    SELECT course_no, description, prerequisite INTO v_courseno, v_course_desc, v_prequisite
    FROM courses
    WHERE course_no = p_courseno;
    
    IF v_prequisite IS NULL THEN
        RETURN 'GEEN';
    ELSE
        SELECT course_no, description INTO v_courseno, v_course_desc
        FROM courses
        WHERE course_no = v_prequisite;
        
        RETURN v_course_desc;
    END IF;
    
    EXCEPTION     
        WHEN e_foutieve_no THEN          
            DBMS_OUTPUT.PUT_LINE('Je hebt een foutieve nummer ingegeven!');
END BEPAAL_VOORVEREISTE_CURSUS;
