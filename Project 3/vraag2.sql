create or replace PROCEDURE MEESTE_AANTAL_INSCHR_CUR IS
    CURSOR cur_inschrijvingen IS
        SELECT e.section_id section_id, c.description description, COUNT(*) aantal
        FROM enrollments e
        JOIN sections s ON (e.section_id = s.section_id)
        JOIN courses c ON (s.course_no = c.course_no)
        GROUP BY e.section_id, c.description
        HAVING COUNT(*) = (SELECT MAX(COUNT(*))
                            FROM enrollments
                            GROUP BY section_id);
BEGIN
    DBMS_OUTPUT.PUT_LINE('section_id description                      aantal');
    DBMS_OUTPUT.PUT_LINE('==================================================');
    
    FOR r_inschrijving IN cur_inschrijvingen LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(r_inschrijving.section_id, 10) || ' ' || RPAD(r_inschrijving.description, 30) ||
            LPAD(RPAD(r_inschrijving.aantal, '4'), 9));
    END LOOP;
END MEESTE_AANTAL_INSCHR_CUR;
