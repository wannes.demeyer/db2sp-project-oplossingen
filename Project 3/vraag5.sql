create or replace PROCEDURE OVERZICHT_RES_MIDTERM_EXAM(
    p_employer IN students.employer%type
) IS
    CURSOR cur_students IS
        SELECT employer, student_id, first_name, last_name
        FROM students
        WHERE LOWER(p_employer) = LOWER(employer);

    CURSOR cur_secties (p_student_id IN students.student_id%type) IS
        SELECT s.section_id, c.description, g.numeric_grade
        FROM sections s
        JOIN courses c ON (s.course_no = c.course_no)
        JOIN grades g ON (s.section_id = g.section_id)
        WHERE g.student_id = p_student_id AND g.grade_type_code = 'MT';

BEGIN
    DBMS_OUTPUT.PUT_LINE('Employer: ' || UPPER(p_employer));
    DBMS_OUTPUT.PUT_LINE('___________________________________');

    FOR r_student IN cur_students LOOP
        DBMS_OUTPUT.PUT_LINE(' ');
        DBMS_OUTPUT.PUT_LINE('Student: ' || r_student.student_id || ' ' ||
            RPAD(r_student.last_name || ' ' || r_student.first_name, 13));
        DBMS_OUTPUT.PUT_LINE('--------------------------');
                
        FOR r_secties IN cur_secties (r_student.student_id) LOOP
            DBMS_OUTPUT.PUT_LINE(r_secties.section_id || ' ' || r_secties.description || ' ' || r_secties.numeric_grade);
        END LOOP;
    END LOOP;
END OVERZICHT_RES_MIDTERM_EXAM;
