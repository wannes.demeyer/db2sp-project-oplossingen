CREATE OR REPLACE PROCEDURE VOEG_INSCHRIJVING_TOE(
    p_student_id    IN enrollments.student_id%TYPE,
    p_section_id    IN enrollments.section_id%TYPE
) IS
BEGIN
    INSERT INTO enrollments
    VALUES(p_student_id, p_section_id, SYSDATE, 0, USER, SYSDATE, USER, SYSDATE);
    
    COMMIT;
    
    DBMS_OUTPUT.PUT_LINE('De inschrijving voor student ' || p_student_id || ' in de sectie ' ||
        p_section_id || ' is succesvol toegevoegd.');
END VOEG_INSCHRIJVING_TOE;
