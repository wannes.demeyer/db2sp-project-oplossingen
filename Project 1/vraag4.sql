create or replace PROCEDURE BEPAAL_VOORVEREISTE_CURSUS(
    p_cursusnummer IN courses.course_no%TYPE
) IS
    v_description   courses.description%TYPE;
    v_pre_id        courses.prerequisite%TYPE;
    v_pre_desc      courses.description%TYPE;
BEGIN
    SELECT description, prerequisite INTO v_description, v_pre_id
    FROM courses
    WHERE course_no = p_cursusnummer;
    
    IF v_pre_id IS NULL THEN
        DBMS_OUTPUT.PUT_LINE('Cursus ' || p_cursusnummer || ' ' || v_description || ' heeft geen voorvereiste cursus.');
    ELSE
        SELECT description INTO v_pre_desc
        FROM courses
        WHERE course_no = v_pre_id;
        
        DBMS_OUTPUT.PUT_LINE('Cursus ' || p_cursusnummer || ' ' || v_description || ' heeft als voorvereiste cursus ' || v_pre_id || ' ' || v_pre_desc || '.');
    END IF;
END BEPAAL_VOORVEREISTE_CURSUS;
