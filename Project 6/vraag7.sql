create or replace TRIGGER TR_AUDIT_GRADES_WEIGHTS
BEFORE INSERT ON GRADES
FOR EACH ROW
DECLARE
    v_max NUMBER;
    v_aantal NUMBER;
BEGIN
    SELECT number_per_section INTO v_max
    FROM grade_type_weights
    WHERE section_id = :NEW.section_id AND grade_type_code = :NEW.grade_type_code;
    
    SELECT COUNT(*) INTO v_aantal
    FROM grades
    WHERE grade_type_code = :NEW.grade_type_code AND student_id = :NEW.student_id AND section_id = :NEW.section_id;
    
    IF v_aantal >= v_max THEN
        RAISE_APPLICATION_ERROR(-20000, 'Student ' || :NEW.student_id || ' heeft al ' || v_aantal || ' ' || :NEW.grade_type_code ||
            ' gemaakt voor sectie ' || :NEW.section_id || '!');
    END IF;
END;
